import { Component, OnInit } from '@angular/core';
import { Cellule } from '../cell';
import { Input } from '@angular/core';
import { isNumber } from '@ng-bootstrap/ng-bootstrap/util/util';
@Component({
  selector: 'app-maze',
  templateUrl: './maze.component.html',
  styleUrls: ['./maze.component.css']
})

export class MazeComponent implements OnInit {

  constructor() {
    // this.generateRandomMaze(this.height, this.width);
  }

  @Input() width = 20;
  @Input() height = 20;
  @Input() runtime = 1;

  solution_plus_courte_trouvee: Cellule[][];
  minCellVisited;
  mean: number;
  randommaze: Cellule[][];
  wnow: number;
  hnow: number;
  wentrance: number;
  hentrance: number;
  wexit: number;
  hexit: number;
  resolved = false;
  selectCell: Cellule;
  count = 0;
  nbCellVisited = 0;
  N = 'N';
  S = 'S';
  E = 'E';
  W = 'W';

  ngOnInit() {
  }

  initMaze(height: number, width: number) {
    console.log('initmaze(' + height + ',' + width + ')');
    this.randommaze = [];
    for (let h = 0; h < height; h++) {
      this.randommaze.push([]);
      for (let w = 0; w < width; w++) {
        this.randommaze[h][w] = {
          portes: ['', '', '', ''], visited: 'false', type: 'cellule', node: 'false', now: '', w: w, h: h,
          visitedTime: 0
        };
      }
    }
    console.log('FIN INIT MAZE');
  }

  reinitMaze(height: number, width: number) {

    for (let h = 0; h < height; h++) {

      for (let w = 0; w < width; w++) {
        this.randommaze[h][w].visited = 'false';
        this.randommaze[h][w].visitedTime = 0;
      }
    }
    this.resolved = true;

  }


  solveMazeStat(algo: string) {
    let res_count;
    let sumcount = 0;
    this.minCellVisited = 1000000000000000;
    for (let k = 0; k < this.runtime; k++) {
      if (algo === "brown") {
        console.log("brown")
        res_count = this.solveMaze();
      } else { res_count = this.solveMazePile() }
      if (this.nbCellVisited < this.minCellVisited) {
        this.minCellVisited = this.nbCellVisited;
        this.solution_plus_courte_trouvee = [];
        this.solution_plus_courte_trouvee = JSON.parse(JSON.stringify(this.randommaze));
      }
      sumcount = sumcount + res_count;
    }
    this.count = sumcount;
    this.randommaze = this.solution_plus_courte_trouvee;
    this.mean = sumcount / this.runtime
  }


  solveMaze(): number {
    console.log("start")

    this.reinitMaze(this.height, this.width);
    let count = 0;
    this.nbCellVisited = 0;
    this.wnow = this.wentrance;
    this.hnow = this.hentrance;
    while (this.wnow !== this.wexit || this.hnow !== this.hexit) {
      count = count + 1;
      this.randommaze[this.hnow][this.wnow].visitedTime += 1;
      const portes = this.randommaze[this.hnow][this.wnow].portes;
      const possibleway: string[] = [];
      for (const porte of portes) {
        if (porte !== '') {
          possibleway.push(porte);
        }
      }
      if (possibleway.length >= 2) {
        this.randommaze[this.hnow][this.wnow].node = 'node';
      }
      if (this.randommaze[this.hnow][this.wnow].visited === 'false') {
        this.randommaze[this.hnow][this.wnow].visited = 'true';
        this.nbCellVisited = this.nbCellVisited + 1;
      }

      this.shuffle(possibleway);
      const dir = possibleway[0];
      this.wnow = this.wnow + this.wmove(dir);
      this.hnow = this.hnow + this.hmove(dir);
      this.randommaze[this.hnow][this.wnow].now = 'now';
    }
    this.resolved = true;
    console.log('SOLVED');
    this.count = count;
    this.mean = count;
    return (count);
  }

  generateRandomMaze(height: number, width: number) {
    console.log('generateRandomMaze(' + height + ',' + width + ')');
    if (isNaN(height) || isNaN(width) || !isNumber(height) || !isNumber(width)) {
      // if ( !(!isNaN(height) && isNumber(+height))&&(!isNaN(width) && isNumber(+width))) {
      alert('Veuillez choisir des entiers pour height et width');
    } else {
      console.log('generateRandomMaze(' + height + ',' + width + ')');
      this.initMaze(height, width);
      let w = this.getRandomInt(width - 1);
      let h = this.getRandomInt(height - 1);
      let remaining = width * height - 1;
      let direction: string[] = [this.N, this.S, this.E, this.W];
      while (remaining > 0) {
        const possibledirection = this.shuffle(direction);
        direction = [this.N, this.S, this.E, this.W];
        for (const dir of possibledirection) {
          const nw = w + this.wmove(dir);
          const nh = h + this.hmove(dir);
          if (nw >= 0 && nh >= 0 && nw < width && nh < height) {
            if (this.randommaze[nh][nw].portes[0] === ''
              && this.randommaze[nh][nw].portes[1] === ''
              && this.randommaze[nh][nw].portes[2] === ''
              && this.randommaze[nh][nw].portes[3] === '') {
              this.randommaze[h][w].portes[direction.indexOf(dir)] = dir;
              this.randommaze[nh][nw].portes[direction.indexOf(this.opp(dir))] = this.opp(dir);
              remaining -= 1;
            }
            w = nw;
            h = nh;
            break;
          }
        }
      }
      this.setEntreeSortie();
    }
  }


  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  wmove(dir: string): number {
    if ((dir === this.N) || (dir === this.S)) {
      return (0);
    }
    if (dir === this.E) {
      return (1);
    }
    if (dir === this.W) {
      return (-1);
    }
  }

  hmove(dir: string): number {
    if ((dir === this.E) || (dir === this.W)) {
      return (0);
    }
    if (dir === this.N) {
      return (-1);
    }
    if (dir === this.S) {
      return (1);
    }
  }

  opp(dir: string): string {
    if (dir === this.N) {
      return this.S;
    }
    if (dir === this.S) {
      return this.N;
    }
    if (dir === this.E) {
      return this.W;
    }
    if (dir === this.W) {
      return this.E;
    }
  }

  shuffle<T>(array: T[]) {
    let m = array.length, t: T, i: number;
    while (m) {
      i = Math.floor(Math.random() * m--);
      t = array[m];
      array[m] = array[i];
      array[i] = t;
    }
    return array;
  }



  setEntreeSortie() {

    console.log('setEntreeSortie');
    let w = this.getRandomInt(this.width);
    let h = this.getRandomInt(this.height);

    this.randommaze[h][w].type = 'entrance';
    this.wnow = w;
    this.hnow = h;
    this.wentrance = w;
    this.hentrance = h;
    w = this.getRandomInt(this.width);
    h = this.getRandomInt(this.height);

    this.randommaze[h][w].type = 'exit';
    this.wexit = w;
    this.hexit = h;
  }

  updateSelectedCell(h: number, w: number) {
    this.selectCell = this.randommaze[h][w];
  }

  solveMazePile(): number {
    console.log("start")
    let pileCellule: Cellule[] = [];
    let pileNode: Cellule[] = []
    this.reinitMaze(this.height, this.width);
    let count = 0;
    this.nbCellVisited = 0;
    this.wnow = this.wentrance;
    this.hnow = this.hentrance;
    while ((this.wnow !== this.wexit || this.hnow !== this.hexit)) {
      count = count + 1;
      this.randommaze[this.hnow][this.wnow].visitedTime += 1;
      const portes = this.randommaze[this.hnow][this.wnow].portes;
      const possibleway: string[] = [];
      const possiblewaynotvisited: string[] = [];
      for (const porte of portes) {
        if (porte !== '') {
          possibleway.push(porte);
        }
      }


      for (const porte of possibleway) {
        if (this.randommaze[this.hnow + this.hmove(porte)][this.wnow + this.wmove(porte)].visited === 'false') {
          possiblewaynotvisited.push(porte);
        }
      }

      if (possiblewaynotvisited.length >= 2) {
        pileNode.push(this.randommaze[this.hnow][this.wnow]);
        this.randommaze[this.hnow][this.wnow].node = 'node';
      }


      if (this.randommaze[this.hnow][this.wnow].visited === 'false') {
        this.randommaze[this.hnow][this.wnow].visited = 'true';
        this.nbCellVisited = this.nbCellVisited + 1;
        pileCellule.push(this.randommaze[this.hnow][this.wnow]);
      }

      if (possiblewaynotvisited.length === 0) {


        this.wnow = pileNode[pileNode.length - 1].w;
        this.hnow = pileNode[pileNode.length - 1].h;
        this.randommaze[this.hnow][this.wnow].now = 'now';
        pileNode.splice(pileNode.length - 1, 1);



      } else {

        this.shuffle(possiblewaynotvisited);
        const dir = possiblewaynotvisited[0];
        this.wnow = this.wnow + this.wmove(dir);
        this.hnow = this.hnow + this.hmove(dir);
        this.randommaze[this.hnow][this.wnow].now = 'now';
      }
    }
    this.resolved = true;
    this.count = count;
    console.log('SOLVED');
    this.mean = count;
    return (count);

  }



}
function compareCelluleWithNow(celluleA: Cellule): boolean {
  return (celluleA.h === this.hnow && celluleA.w === this.wnow);
}
