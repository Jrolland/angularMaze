import { Component } from '@angular/core';
import shuffle from 'shuffle.ts';
import { Cellule } from './cell';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

}
