export class Cellule {
    w: number;
    h: number;
    portes: string[] = ['', '', '', ''];
    visited = 'false';
    type: string;
    node = 'false';
    now = '';
    visitedTime = 0;

    constructor(portes, visited, type) {
        this.portes = portes;
        this.visited = visited;
        this.type = type;

    }

}
