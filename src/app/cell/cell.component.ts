import { Component, OnInit } from '@angular/core';
import {Input} from '@angular/core';
import { Cellule } from '../cell';

@Component({
  selector: 'app-cell',
  templateUrl: './cell.component.html',
  styleUrls: ['./cell.component.css']
})
export class CellComponent implements OnInit {

  constructor() { }
  @Input() cellule : Cellule;
  ngOnInit() {
  }

}
