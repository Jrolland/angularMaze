import { Cellule } from "./cell";
import { isNumber } from "util";

export class Maze {
    width: number;
    height: number;
    solved: boolean;
    wentrance: number;
    hentrance: number;
    wexit: number;
    hexit: number;
    wnow: number;
    hnow: number;
    cells: Cellule[][];
    count: number;

    constructor(height, width) {
        this.height = height;
        this.width = width;

    }
    generatecells(height: number, width: number) {
        if (isNaN(height) || isNaN(width) || !isNumber(height) || !isNumber(width)) {
            // if ( !(!isNaN(height) && isNumber(+height))&&(!isNaN(width) && isNumber(+width))) {
            alert('Veuillez choisir des entiers pour height et width');

        } else {
            console.log('generatecells(' + height + ',' + width + ')');
            this.initMaze();

            let w = this.getRandomInt(width - 1);
            let h = this.getRandomInt(height - 1);
            let remaining = width * height - 1;
            let direction: string[] = ['N', 'S', 'E', 'W'];

            while (remaining > 0) {


                const possibledirection = this.shuffle(direction);
                direction = ['N', 'S', 'E', 'W'];
                for (const dir of possibledirection) {

                    const nw = w + this.wmove(dir);
                    const nh = h + this.hmove(dir);


                    if (nw >= 0 && nh >= 0 && nw < width && nh < height) {

                        if (this.cells[nh][nw].portes[0] === ''
                            && this.cells[nh][nw].portes[1] === ''
                            && this.cells[nh][nw].portes[2] === ''
                            && this.cells[nh][nw].portes[3] === '') {


                            this.cells[h][w].portes[direction.indexOf(dir)] = dir;
                            this.cells[nh][nw].portes[direction.indexOf(this.opp(dir))] = this.opp(dir);

                            remaining -= 1;
                        }
                        w = nw;
                        h = nh;
                        break;
                    }
                }
            }
            this.setEntreeSortie();
        }
    }

    getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }
    wmove(dir: string): number {
        if ((dir === 'N') || (dir === 'S')) {
            return (0);
        }
        if (dir === 'E') {
            return (1);
        }
        if (dir === 'W') {
            return (-1);
        }
    }

    hmove(dir: string): number {
        if ((dir === 'E') || (dir === 'W')) {
            return (0);
        }
        if (dir === 'N') {
            return (-1);
        }
        if (dir === 'S') {
            return (1);
        }
    }
    opp(dir: string): string {
        if (dir === 'N') {
            return 'S';
        }
        if (dir === 'S') {
            return 'N';
        }
        if (dir === 'E') {
            return 'W';
        }
        if (dir === 'W') {
            return 'E';
        }
    }
    shuffle<T>(array: T[]) {
        let m = array.length, t: T, i: number;
        while (m) {
            i = Math.floor(Math.random() * m--);
            t = array[m];
            array[m] = array[i];
            array[i] = t;
        }
        return array;
    }
    sleep(milliseconds) {
        const start = new Date().getTime();
        for (let i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds) {
                break;
            }
        }
    }

    setEntreeSortie() {
        console.log(this.cells);
        console.log('setEntreeSortie');
        let w = this.getRandomInt(this.width - 1);
        let h = this.getRandomInt(this.height - 1);
        console.log(w);
        console.log(h);
        this.cells[h][w].type = 'entrance';
        this.wnow = w;
        this.hnow = h;
        this.wentrance = w;
        this.hentrance = h;

        w = this.getRandomInt(this.width - 1);
        h = this.getRandomInt(this.height - 1);
        console.log(w);
        console.log(h);
        this.cells[h][w].type = 'exit';
        this.wexit = w;
        this.hexit = h;
    }

    reinitMaze() {

        for (let h = 0; h < this.height; h++) {
            this.cells.push([]);
            for (let w = 0; w < this.width; w++) {
                this.cells[h][w].visited = 'false';
            }
        }
        console.log('FIN INIT MAZE');

    }

    public solveMaze() {
        this.reinitMaze();
        this.count = 0;
        this.wnow = this.wentrance;
        this.hnow = this.hentrance;
        while (this.wnow !== this.wexit || this.hnow !== this.hexit) {
            this.count = this.count + 1;
            this.cells[this.hnow][this.wnow].visitedTime += 1;
            const portes = this.cells[this.hnow][this.wnow].portes;
            const possibleway: string[] = [];
            for (const porte of portes) {
                if (porte !== '') {
                    possibleway.push(porte);
                }
            }

            if (possibleway.length >= 2) {
                this.cells[this.hnow][this.wnow].node = 'node';
            }
            this.cells[this.hnow][this.wnow].visited = 'true';
            this.shuffle(possibleway);

            const dir = possibleway[0];

            this.wnow = this.wnow + this.wmove(dir);
            this.hnow = this.hnow + this.hmove(dir);
            this.cells[this.hnow][this.wnow].now = 'now';

        }
        console.log('SOLVED');
    }


    initMaze() {
        console.log('initmaze(' + this.height + ',' + this.width + ')');


        this.cells = [];

        for (let h = 0; h < this.height; h++) {

            this.cells.push([]);
            for (let w = 0; w < this.width; w++) {



                this.cells[h][w] = {
                    portes: ['', '', '', ''], visited: 'false', type: 'cellule', node: 'false', now: '', w: w, h: h,
                    visitedTime: 0
                };

            }

        }

        console.log('FIN INIT MAZE');

    }
}
