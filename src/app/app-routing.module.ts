import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MazeComponent } from './maze/maze.component';
import { ManualMazeComponent } from './manual-maze/manual-maze.component';
import { StatComponent } from './stat/stat.component';


const routes: Routes = [
  { path: '', component: MazeComponent },
  { path: 'maze', component: MazeComponent },

  { path: 'manualmaze', component: ManualMazeComponent },
  { path: 'stat', component: StatComponent }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})


export class AppRoutingModule { }
