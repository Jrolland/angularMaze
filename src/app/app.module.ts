import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { MazeComponent } from './maze/maze.component';
import { CellComponent } from './cell/cell.component';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { StatComponent } from './stat/stat.component';
import { ManualMazeComponent } from './manual-maze/manual-maze.component';
import { AppRoutingModule } from './/app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    MazeComponent,
    CellComponent,
    HomeComponent,
    StatComponent,
    ManualMazeComponent
  ],
  imports: [
    BrowserModule, NgbModule.forRoot(), FormsModule, AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
